$(function () {
    var bigList = $('.title li');
    var lis = $('.smallList li');
    var contents1 = $('.useTry .content');
    var contents2 = $('.vip .content');
    
    var n = 0;
    $.each(lis, function (index, item) {
        $(item).click(function () {
            $(this).attr('class', 'smallCurrent');
            $(this).siblings().attr('class',null);
            n = $(this).index();
            // console.log(n);
            // console.log(contents.eq(n));
            for(var m = 0; m < lis.length; m++){
                contents1.eq(m).css('display','none');
                // console.log(contents.eq(m));
            }
            contents1.eq(n).css('display','block');
        });
    });

    var i = 0;
    $.each(bigList,function(i,ele){
        $(ele).click(function () {
            $(this).attr('class', 'current');
            $(this).siblings().attr('class',null);
            i = $(this).index();
            for(var j = 0; j < bigList.length; j++){
                contents2.eq(j).css('display','none');
            }
            contents2.eq(i).css('display','block');
        });
    });
});