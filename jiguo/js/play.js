$(function () {
    var lis = $('.title li');
    var contents = $('.content');
    var n = 0;
    $.each(lis, function (index, item) {
        $(item).click(function () {
            $(this).prop('class', 'current');
            $(this).siblings().attr('class',null);
            n = $(this).index();
            // console.log(n);
            // console.log(contents.eq(n));
            for(var m = 0; m < lis.length; m++){
                contents.eq(m).css('display','none');
            }
            contents.eq(n).css('display','block');
        });
    });
});