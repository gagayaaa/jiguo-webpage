const first_public = document.getElementsByClassName('first_public')[0];

function getData() {
    // 1、创建对象
    var myAjax_ = new XMLHttpRequest() || new ActiveXObject('Microsoft.XMLHTT');
    //2、建立连接
    myAjax_.open('get', 'http://localhost:3000/useing/master', true);
    //3、发送请求
    myAjax_.send();
    //4、接收服务器数据
    myAjax_.onreadystatechange = function () {
        if (myAjax_.readyState == 4) {
            // 成功
            if (myAjax_.status == 200) {
                // console.log(ajax_.responseText);//到这里已经获取了数据，但格式是字符串，但需要转换
                var res = JSON.parse(myAjax_.responseText);//此时数据的格式为数组
                // console.log(res);

                for (var i = 0; i < res.length; i++) {
                    for (var j = 0; j < res[i].length; j++) {
                        var img = document.createElement('img');
                        img.src = res[i][j].img;
                        document.body.appendChild(img);

                        var p = document.createElement('p');
                        p.innerHTML = res[i][j].text;
                        document.body.appendChild(p);
                    }
                }
            } else {
                // 失败
                console.log('获取连接失败');
            }
        }
    }
}
getData();