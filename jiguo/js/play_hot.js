const playHot = document.getElementsByClassName('playHot')[0];
const ul = playHot.querySelector('ul');


function getData() {
    // 1、创建对象
    var myAjax_ = new XMLHttpRequest() || new ActiveXObject('Microsoft.XMLHTT');
    //2、建立连接
    myAjax_.open('get', 'http://localhost:3000/play/hot', true);
    //3、发送请求
    myAjax_.send();
    //4、接收服务器数据
    myAjax_.onreadystatechange = function () {
        if (myAjax_.readyState == 4) {
            // 成功
            if (myAjax_.status == 200) {
                // console.log(ajax_.responseText);//到这里已经获取了数据，但格式是字符串，但需要转换
                var res = JSON.parse(myAjax_.responseText);//此时数据的格式为数组
                // console.log(res);

                for (var i = 0; i < res.length; i++) {
                    for (var j = 0; j < res[i].length; j++) {
                        var li = document.createElement('li');
                        ul.appendChild(li);

                        var img = document.createElement('img');
                        img.src = res[i][j].img;
                        li.appendChild(img);

                        var p1 = document.createElement('p');
                        p1.innerHTML = res[i][j].text;
                        li.appendChild(p1);

                        var p2 = document.createElement('p');
                        p2.innerHTML = res[i][j].description;
                        li.appendChild(p2);

                        var p3 = document.createElement('p');
                        li.appendChild(p3);
                        var span1 = document.createElement('span');
                        span1.innerHTML = res[i][j].price;
                        span1.className = 'price';
                        p3.appendChild(span1);
                        var span2 = document.createElement('span');
                        span2.innerHTML = '<img src="../img/reply.png"> ' + res[i][j].words;
                        span2.className = 'reply';
                        var img = document.createElement('img');
                        img.src = '../img/reply.png';
                        p3.appendChild(span2);
                        var span3 = document.createElement('span');
                        span3.innerHTML = '<img src="../img/xin.png"> '+res[i][j].like;
                        span3.className = 'xin';
                        p3.appendChild(span3);
                    }
                }
            } else {
                // 失败
                console.log('获取连接失败');
            }
        }
    }
}
getData();