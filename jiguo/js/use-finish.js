const finish = document.getElementsByClassName('finish')[0];
const finish_ul = finish.querySelector('ul');


function getData() {
    // 1、创建对象
    var myAjax_ = new XMLHttpRequest() || new ActiveXObject('Microsoft.XMLHTT');
    //2、建立连接
    myAjax_.open('get', 'http://localhost:3000/useing/public', true);
    //3、发送请求
    myAjax_.send();
    //4、接收服务器数据
    myAjax_.onreadystatechange = function () {
        if (myAjax_.readyState == 4) {
            // 成功
            if (myAjax_.status == 200) {
                // console.log(ajax_.responseText);//到这里已经获取了数据，但格式是字符串，但需要转换
                var res = JSON.parse(myAjax_.responseText);//此时数据的格式为数组
                // console.log(res);

                for (var i = 0; i < res.length-1; i++) {
                    var li = document.createElement('li');
                    finish_ul.appendChild(li);

                    var img = document.createElement('img');
                    img.src = res[i].img;
                    li.appendChild(img);

                    var p1 = document.createElement('p');
                    p1.innerHTML = res[i].text;
                    p1.className = 'describe';
                    li.appendChild(p1);

                    var p2 = document.createElement('p');
                    p2.className = 'num';
                    li.appendChild(p2);
                    var span1 = document.createElement('span');
                    span1.innerHTML = res[i].totalnum;
                    // span1.className = 'totalnum';
                    p2.appendChild(span1);

                    var span2 = document.createElement('span');
                    span2.innerHTML = res[i].num+'台';
                    p2.appendChild(span2);

                    var p3 = document.createElement('p');
                    li.appendChild(p3);
                    var span3 = document.createElement('span');
                    span3.className = 'apply'
                    span3.innerHTML = res[i].apply+'申请';
                    p3.appendChild(span3);

                    if(res[i].info_ty == '首发'){
                        var p4 = document.createElement('p');
                        p4.className = 'time';
                        p4.innerHTML = '剩余时间2天';
                        li.appendChild(p4);
                        li.className = 'first_public';
                    }else if(res[i].info_ty == '体验师转享'){
                        var p4 = document.createElement('p');
                        p4.className = 'time';
                        p4.innerHTML = '查看试用名单';
                        li.appendChild(p4);
                        li.className = 'taste';
                    }
                }
                // all_ul.remove('li:last-of-type');
            }
        } else {
            // 失败
            // console.log('获取连接失败');
        }
    }
}
getData();