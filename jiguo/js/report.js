const reportNew = document.getElementsByClassName('reportNew')[0];
const reportNew_ul = reportNew.querySelector('ul');


function getData() {
    // 1、创建对象
    var myAjax_ = new XMLHttpRequest() || new ActiveXObject('Microsoft.XMLHTT');
    //2、建立连接
    myAjax_.open('get', 'http://localhost:3000/report/new', true);
    //3、发送请求
    myAjax_.send();
    //4、接收服务器数据
    myAjax_.onreadystatechange = function () {
        if (myAjax_.readyState == 4) {
            // 成功
            if (myAjax_.status == 200) {
                // console.log(ajax_.responseText);//到这里已经获取了数据，但格式是字符串，但需要转换
                var res = JSON.parse(myAjax_.responseText);//此时数据的格式为数组
                // console.log(res);

                for (var i = 0; i < res.length-1; i++) {
                    var li = document.createElement('li');
                    reportNew_ul.appendChild(li);

                    var img = document.createElement('img');
                    img.src = res[i].img;
                    li.appendChild(img);

                    var div_top = document.createElement('div');
                    div_top.className = 'top';
                    li.appendChild(div_top);

                    var p1 = document.createElement('p');
                    p1.innerHTML = res[i].text;
                    p1.className = 'describe';
                    div_top.appendChild(p1);

                    var p3 = document.createElement('p');
                    p3.className = 'user';
                    div_top.appendChild(p3);
                    
                    var span1 = document.createElement('span');
                    span1.className = 'uname';
                    span1.innerHTML = ' '+res[i].uName;
                    p3.appendChild(span1);

                    var span2 = document.createElement('span');
                    span2.innerHTML = res[i].endTime;
                    span2.className = 'time';
                    p3.appendChild(span2);

                    var span3 = document.createElement('span');
                    span3.innerHTML = res[i].apply;
                    span3.className = 'reply';
                    p3.appendChild(span3);

                    var span4 = document.createElement('span');
                    span4.innerHTML = res[i].num;
                    span4.className = 'zan';
                    p3.appendChild(span4);
                    

                    var p2 = document.createElement('p');
                    p2.className = 'txt';
                    p2.innerHTML = '关于格林维特净化器还有8片报告，点击查看';
                    li.appendChild(p2);
                }
            }
        } else {
            // 失败
            // console.log('获取连接失败');
        }
    }
}
getData();

const reportHot = document.getElementsByClassName('reportHot')[0];
const reportHot_ul = reportHot.querySelector('ul');


function getData2() {
    // 1、创建对象
    var myAjax_ = new XMLHttpRequest() || new ActiveXObject('Microsoft.XMLHTT');
    //2、建立连接
    myAjax_.open('get', 'http://localhost:3000/report/hot', true);
    //3、发送请求
    myAjax_.send();
    //4、接收服务器数据
    myAjax_.onreadystatechange = function () {
        if (myAjax_.readyState == 4) {
            // 成功
            if (myAjax_.status == 200) {
                // console.log(ajax_.responseText);//到这里已经获取了数据，但格式是字符串，但需要转换
                var res = JSON.parse(myAjax_.responseText);//此时数据的格式为数组
                // console.log(res);

                for (var i = 0; i < res.length-1; i++) {
                    var li = document.createElement('li');
                    reportHot_ul.appendChild(li);

                    var img = document.createElement('img');
                   img.src = res[i].img;
                    li.appendChild(img);

                    var div_top = document.createElement('div');
                    div_top.className = 'top';
                    li.appendChild(div_top);

                    var p1 = document.createElement('p');
                    p1.innerHTML = res[i].text;
                    p1.className = 'describe';
                    div_top.appendChild(p1);

                    var p3 = document.createElement('p');
                    p3.className = 'user';
                    div_top.appendChild(p3);
                    
                    var span1 = document.createElement('span');
                    span1.className = 'uname';
                    span1.innerHTML = ' '+res[i].uName;
                    p3.appendChild(span1);

                    var span2 = document.createElement('span');
                    span2.innerHTML = res[i].endTime;
                    span2.className = 'time';
                    p3.appendChild(span2);

                    var span3 = document.createElement('span');
                    span3.innerHTML = res[i].apply;
                    span3.className = 'reply';
                    p3.appendChild(span3);

                    var span4 = document.createElement('span');
                    span4.innerHTML = res[i].num;
                    span4.className = 'zan';
                    p3.appendChild(span4);
                    
                }
            }
        } else {
            // 失败
            // console.log('获取连接失败');
        }
    }
}
getData2();


$(function () {
    var lis = $('.title li');
    var contents = $('.left');
    var n = 0;
    $.each(lis, function (index, item) {
        $(item).click(function () {
            $(this).prop('class', 'current');
            $(this).siblings().attr('class',null);
            n = $(this).index();
            // console.log(n);
            // console.log(contents.eq(n));
            for(var m = 0; m < lis.length; m++){
                contents.eq(m).css('display','none');
            }
            contents.eq(n).css('display','block');
        });
    });
});