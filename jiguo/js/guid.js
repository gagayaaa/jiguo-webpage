const guidNew = document.getElementsByClassName('guidNew')[0];
const guidNew_ul = guidNew.querySelector('ul');
// console.log(guidNew_ul);

function getData1() {
    // 1、创建对象
    var myAjax_ = new XMLHttpRequest() || new ActiveXObject('Microsoft.XMLHTT');
    //2、建立连接
    myAjax_.open('get', 'http://localhost:3000/guid/new', true);
    //3、发送请求
    myAjax_.send();
    //4、接收服务器数据
    myAjax_.onreadystatechange = function () {
        if (myAjax_.readyState == 4) {
            // 成功
            if (myAjax_.status == 200) {
                // console.log(ajax_.responseText);//到这里已经获取了数据，但格式是字符串，但需要转换
                var res = JSON.parse(myAjax_.responseText);//此时数据的格式为数组
                // console.log(res);

                for (var i = 0; i < res.length; i++) {
                    var li1 = document.createElement('li');
                    guidNew_ul.appendChild(li1);
                    var li_p1 = document.createElement('p');
                    li_p1.className = 'center';
                    li1.appendChild(li_p1);
                    var li_p2 = document.createElement('p');
                    li_p2.className = 'bottom';
                    li1.appendChild(li_p2);

                    var img = document.createElement('img');
                    img.src = res[i].img;
                    li1.appendChild(img);

                    var p1 = document.createElement('p');
                    p1.innerHTML = res[i].text;
                    li1.appendChild(p1);

                    var p3 = document.createElement('p');
                    li1.appendChild(p3);

                    var span2 = document.createElement('span');
                    span2.innerHTML = '<img src="../img/reply.png"> ' + res[i].words;
                    span2.className = 'reply';
                    p3.appendChild(span2);
                    var span3 = document.createElement('span');
                    span3.innerHTML = '<img src="../img/xin.png"> ' + res[i].like;
                    span3.className = 'xin';
                    p3.appendChild(span3);
                }
            }
        } else {
            // 失败
            // console.log('获取连接失败');
        }
    }
}
getData1();


const guidHot = document.getElementsByClassName('guidHot')[0];
const guidHot_ul = guidHot.querySelector('ul');
// console.log(guidNew_ul);

function getData2() {
    // 1、创建对象
    var myAjax_ = new XMLHttpRequest() || new ActiveXObject('Microsoft.XMLHTT');
    //2、建立连接
    myAjax_.open('get', 'http://localhost:3000/guid/hot', true);
    //3、发送请求
    myAjax_.send();
    //4、接收服务器数据
    myAjax_.onreadystatechange = function () {
        if (myAjax_.readyState == 4) {
            // 成功
            if (myAjax_.status == 200) {
                // console.log(ajax_.responseText);//到这里已经获取了数据，但格式是字符串，但需要转换
                var res = JSON.parse(myAjax_.responseText);//此时数据的格式为数组
                // console.log(res);

                for (var i = 0; i < res.length; i++) {
                    var li1 = document.createElement('li');
                    guidHot_ul.appendChild(li1);
                    var li_p1 = document.createElement('p');
                    li_p1.className = 'center';
                    li1.appendChild(li_p1);
                    var li_p2 = document.createElement('p');
                    li_p2.className = 'bottom';
                    li1.appendChild(li_p2);

                    var img = document.createElement('img');
                    img.src = res[i].img;
                    li1.appendChild(img);

                    var p1 = document.createElement('p');
                    p1.innerHTML = res[i].text;
                    li1.appendChild(p1);

                    var p3 = document.createElement('p');
                    li1.appendChild(p3);

                    var span2 = document.createElement('span');
                    span2.innerHTML = '<img src="../img/reply.png"> ' + res[i].words;
                    span2.className = 'reply';
                    p3.appendChild(span2);
                    var span3 = document.createElement('span');
                    span3.innerHTML = '<img src="../img/xin.png"> ' + res[i].like;
                    span3.className = 'xin';
                    p3.appendChild(span3);
                }
            }
        } else {
            // 失败
            // console.log('获取连接失败');
        }
    }
}
getData2();

$(function () {
    var lis = $('.title li');
    var contents = $('.content');
    var n = 0;
    $.each(lis, function (index, item) {
        $(item).click(function () {
            $(this).prop('class', 'current');
            $(this).siblings().attr('class',null);
            n = $(this).index();
            // console.log(n);
            // console.log(contents.eq(n));
            for(var m = 0; m < lis.length; m++){
                contents.eq(m).css('displsy','none');
            }
            contents.eq(n).css('displsy','block');
        });
    });
});
