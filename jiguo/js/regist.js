window.onload = function () {
    const phoneNum = document.getElementById('phoneNum');
    const picStr = document.getElementById('picStr');
    const fourNum = document.getElementById('fourNum');
    const btn1 = document.getElementById('getmsg');
    const username = document.getElementById('username');
    const pwd = document.getElementById('pwd');
    const repwd = document.getElementById('repwd');

    // var arr = [];

    // 判断手机号的正则
    const regexp_phoneNum = /^1\d{10}$/;
    // console.log(regexp_phoneNum.test(13345678910));
    phoneNum.onblur = function () {
        var num = phoneNum.value;
        if (!regexp_phoneNum.test(num)) {
            phoneNum.style.borderColor = 'red';
        } else {
            phoneNum.style.borderColor = 'gray';
        }
    }

    // 判断图片校验码
    const regexp_picStr = /^[rR][2][bB][7]$/;
    picStr.onblur = function(){
        var str = picStr.value;
        if(!regexp_picStr.test(str)){
            picStr.style.borderColor = 'red';
        }else{
            picStr.style.borderColor = 'gray';
        }
    }

    // 四位数字验证码
    const regexp_fourNum = /^\d{4}$/;
    fourNum.onblur = function(){
        var fourNum_val = fourNum.value;
        if(!regexp_fourNum.test(fourNum_val)){
            fourNum.style.borderColor = 'red';
        }else{
            fourNum.style.borderColor = 'gray';
        }
    }

    // 获取验证码
    var n = 60;
    // 定时器
    var timer1 = null;
    btn1.onclick = function () {
        btn1.disabled = true;
        btn1.value = '获取验证码(' + n + ')';
        timer1 = setInterval(function () {
            n--;
            btn1.value = '获取验证码(' + n + ')';
            if (n < 0) {
                btn1.value = '重新获取验证码';
                n = 10;
                clearInterval(timer1);
                btn1.disabled = false;
            }
        }, 1000);
    }

    // 判断用户名  字母数字 下划线汉字4-8位
    // var regexp_uName = /^[/u4e00-/u9fa5]+$/;
    // console.log(regexp_uName.test('asdf'));

    // 判断密码
    // 数字或字母或下划线6-12
    var regexp_pwd = /^\w{6,12}$/;
    pwd.onblur = function(){
        var pwd_val = pwd.value;
        if(!regexp_pwd.test(pwd_val)){
            pwd.style.borderColor = 'red';
        }else{
            pwd.style.borderColor = 'gray';
        }
    }

    // 判断确认密码
    repwd.onblur = function(){
        if(repwd.value != pwd.value){
            repwd.style.borderColor = 'red';
        }else{
            repwd.style.borderColor = 'gray';
        }
    }
}